import React, { useState, useEffect, createContext } from "react";

export const PokemonContext = createContext();

export const PokemonProvider = (props) => {
  const [allPokemons, setAllPokemons] = useState([]);
  const [loadMore, setLoadMore] = useState(
    "https://pokeapi.co/api/v2/pokemon?limit=20"
  );

  const getAllPokemons = async () => {
    const res = await fetch(loadMore);
    const data = await res.json();

    setLoadMore(data.next);

    const createPokemon = (result) => {
      result.forEach(async (pokemon) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
        );
        const data = await res.json();

        setAllPokemons((currentPokemonList) => [...currentPokemonList, data]);
      });
    };
    createPokemon(data.results);
    // await console.log(allPokemons)
  };

  useEffect(() => {
    getAllPokemons();
  }, []);

  return (
    <PokemonContext.Provider value={[allPokemons, getAllPokemons]}>
      {props.children}
    </PokemonContext.Provider>
  );
};
