import React from "react";
import { Link } from "react-router-dom";

const PokemonStatus = ({ stats }) => {
  return (
    <>
      <h1> {stats.name}</h1>
      <img
        src={stats.sprites.other.dream_world.front_default}
        alt={stats.name}
        className="pokemonImage"
      ></img>
      <div className="ablities">
        <h2>
          Lagay mo nalang yung mga Stats dito tapos design ka ng UI mo para ma
          view mo mga stats. naka store sa pokemon object yung mga data. Check
          mo Console dito naka console log yung data.
        </h2>
        <h3>Example:</h3>
        <h4>
          <span style={{ color: "red" }}>Weight:</span> {stats.weight}
        </h4>
        <h4>
          <span style={{ color: "red" }}>Height:</span> {stats.height}
        </h4>
      </div>
      <Link to={"../"}>
        <button>Go Back</button>
      </Link>
    </>
  );
};

export default PokemonStatus;
