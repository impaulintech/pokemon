import React from "react";
import { Container, CardGroup } from "react-bootstrap";
import PokemonList from "../Components/PokemonList";

const Homepage = () =>{

    return(
    <React.Fragment>
        <div className="pokedexBody">
            <Container fluid>
                <h1 className="title">Pokedex</h1>
                <CardGroup>
                    <PokemonList />
                </CardGroup>
            </Container>
        </div>
    </React.Fragment>
    );
}

export default Homepage;