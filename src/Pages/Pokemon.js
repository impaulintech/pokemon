import PokemonStatus from "../Components/PokemonStatus";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

const Pokemon = () => {
  const { name, id } = useParams();
  const [pokemon, setPokemon] = useState();
  useEffect(() => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
      .then((res) => res.json())
      .then((data) => setPokemon(data));
  }, []);
  return (
    <>
      {pokemon ? (
        <>
          <PokemonStatus stats={pokemon} />
          {console.log(pokemon)}
        </>
      ) : (
        "Loading page..."
      )}
    </>
  );
};

export default Pokemon;
